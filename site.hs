import safe Data.Monoid (mappend)
import      Hakyll.Core.Compiler
    ( getResourceBody
    , loadAll
    , makeItem )
import safe Hakyll.Core.Configuration
    ( Configuration
    , defaultConfiguration
    , destinationDirectory )
import      Hakyll.Core.File
    ( copyFileCompiler )
import      Hakyll.Core.Identifier.Pattern
    ( fromList )
import      Hakyll.Core.Routes
    ( idRoute
    , setExtension )
import      Hakyll.Core.Rules
    ( compile
    , create
    , match
    , route)
import      Hakyll.Main
    ( hakyllWith )
import      Hakyll.Web.CompressCss
    ( compressCssCompiler )
import      Hakyll.Web.Html.RelativizeUrls
    ( relativizeUrls )
import      Hakyll.Web.Pandoc
    ( pandocCompiler )
import      Hakyll.Web.Template.Context
    ( constField
    , Context
    , dateField
    , defaultContext
    , listField)
import      Hakyll.Web.Template.Internal
    ( applyAsTemplate
    , loadAndApplyTemplate
    , templateBodyCompiler ) --
import      Hakyll.Web.Template.List
    ( recentFirst )

import safe Prelude
    ( ($)
    , (>>=)
    , (=<<)
    , IO
    , return
    , String
    )
-------------------------------------
config :: Configuration
config = defaultConfiguration
  { destinationDirectory = "public"
  }

main :: IO ()
main = hakyllWith config $ do
    match "images/*" $ do
        route   idRoute
        compile copyFileCompiler
    match "css/*" $ do
        route   idRoute
        compile compressCssCompiler
    match (fromList ["about.rst", "contact.markdown"]) $ do
        route   $ setExtension "html"
        compile $ pandocCompiler
            >>= loadAndApplyTemplate "templates/default.html" defaultContext
            >>= relativizeUrls
    match "posts/*" $ do
        route $ setExtension "html"
        compile $ pandocCompiler
            >>= loadAndApplyTemplate "templates/post.html"    postCtx
            >>= loadAndApplyTemplate "templates/default.html" postCtx
            >>= relativizeUrls
    create ["archive.html"] $ do
        route idRoute
        compile $ do
            posts <- recentFirst =<< loadAll "posts/*"
            let archiveCtx =
                    listField "posts" postCtx (return posts) `mappend`
                    constField "title" "Archives"            `mappend`
                    defaultContext
            makeItem ""
                >>= loadAndApplyTemplate "templates/archive.html" archiveCtx
                >>= loadAndApplyTemplate "templates/default.html" archiveCtx
                >>= relativizeUrls
    match "index.html" $ do
        route idRoute
        compile $ do
            posts <- recentFirst =<< loadAll "posts/*"
            let indexCtx =
                    listField "posts" postCtx (return posts) `mappend`
                    defaultContext
            getResourceBody
                >>= applyAsTemplate indexCtx
                >>= loadAndApplyTemplate "templates/default.html" indexCtx
                >>= relativizeUrls
    match "templates/*" $ compile templateBodyCompiler
-------------------------------------
postCtx :: Context String
postCtx =
    dateField "date" "%B %e, %Y" `mappend`
    defaultContext
