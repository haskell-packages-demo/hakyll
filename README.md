# hakyll

S[R](https://repology.org/project/haskell:hakyll/versions)ArDeFeNi Static website compiler library

* https://haskell-packages-demo.gitlab.io/hakyll/archive.html
---
* https://jaspervdj.be/hakyll/
* [stack**overflow** questions tagged [hakyll]](https://stackoverflow.com/questions/tagged/hakyll)
* https://gitlab.com/pages/hakyll

# Static website compilers
![Debian popcon](https://qa.debian.org/cgi-bin/popcon-png?packages=hugo+mkdocs+jekyll+ikiwiki+pelican+nanoc+chronicle+libghc-hakyll-dev+blag&show_installed=1&show_vote=0&show_old=0&show_recent=0&show_nofiles=0&want_percent=0&want_legend=1&want_ticks=1&from_date=&to_date=&hlght_date=&date_fmt=%25Y)
![Debian popcon](https://qa.debian.org/cgi-bin/popcon-png?packages=nanoc+chronicle+libghc-hakyll-dev+blag&show_installed=1&show_vote=0&show_old=0&show_recent=0&show_nofiles=0&want_percent=0&want_legend=1&want_ticks=1&from_date=&to_date=&hlght_date=&date_fmt=%25Y)
* Hugo
* Mkdocs
* Jekyll
* Pelican
* Nanoc
* Ikiwiki
* Chronicle
* Hakyll
* Blag
---
* [Zola](https://repology.org/project/zola)
* Middleman (Used by GitLab, 2021)
* tightenco/jigsaw 
* Mdblog
* Cobalt-bin
* Sculpin
* Spress
* Static-site-generator-webpack-plugin
* Assemble
* Nikola
* Wintersmith
* Metalsmith
---
* https://gitlab.com/javascript-packages-demo/staticsitegenerators
